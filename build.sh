#!/bin/sh
set -e # Exit with nonzero exit code if anything fails
set -x # Display commands and their arguments as they are executed.

wget https://raw.githubusercontent.com/alpinelinux/alpine-make-vm-image/v0.6.0/alpine-make-vm-image -O alpine-make-vm-image
echo 'a0fc0b4886b541bb4dd7b91b4e1e99719a1700da  alpine-make-vm-image' | sha1sum -c || exit 1
chmod +x alpine-make-vm-image

FILENAME=alpine-virthardened-$(date +%Y-%m-%d).${IMAGE_FORMAT}

echo "CREATING: $FILENAME"
sudo ./alpine-make-vm-image \
  --image-size 2G \
  --image-format ${IMAGE_FORMAT} \
  --repositories-file ./repositories \
  --packages "$(cat ./packages)" \
  --script-chroot ${FILENAME} -- ./configure.sh

echo "CREATED: $FILENAME"
echo "CREATING: ${FILENAME}.tar.gz"
tar -zcvf ${FILENAME}.tar.gz ${FILENAME}
echo "CREATED: ${FILENAME}.tar.gz"
